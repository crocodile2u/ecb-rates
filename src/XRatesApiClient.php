<?php
declare(strict_types=1);

namespace ECBRates;

class XRatesApiClient
{
    const BASE_URL = "https://api.exchangeratesapi.io/";

    public function latest(): RateSet
    {
        return $this->run("latest");
    }

    public function on(\DateTime $dateTime)
    {
        return $this->run($dateTime->format("Y-m-d"));
    }

    public function onPreviousBusinessDay(\DateTime $dateTime)
    {
        $dayAgo = $dateTime->sub(new \DateInterval("P1D"));
        return $this->run($dayAgo->format("Y-m-d"));
    }

    private function run(string $path)
    {
        $url = self::BASE_URL . $path;
        $json = file_get_contents($url);
        if (!$json) {
            throw new \RuntimeException("Exchange rates API is not available");
        }

        $decoded = json_decode($json, true);
        if (!$decoded) {
            throw new \RuntimeException("Unexpected response from exchange rates API");
        }

        return new RateSet($decoded["date"], $decoded["rates"], $decoded["base"]);
    }
}