<?php
declare(strict_types=1);

namespace ECBRates;

class CLI
{
    public static function run($event): void
    {
        echo "USAGE: composer rates -- [date]\n";

        /** @var string[] $args */
        $args = $event->getArguments();

        if (count($args)) {
            $method = "on";
            $date = $args[0] ?: "now";
            $desc = "Getting rates for date=$date or the previous business day";
            if ("-" === substr($date, 0, 1)) {
                $date = substr($date, 1) ?: "now";
                $method = "onPreviousBusinessDay";
                $desc = "Getting rates for the previous business day with date=$date";
            }
            $methodArgs = [new \DateTime($date)];
        } else {
            $method = "latest";
            $methodArgs = [];
            $desc = "Getting latest available rates";
        }

        $client = new XRatesApiClient();
        $rs = $client->$method(...$methodArgs);

        echo "$desc\n";
        echo "EUR exchange rates from ECB for {$rs->date()}:\n";
        echo json_encode($rs->all(), JSON_PRETTY_PRINT);
    }
}