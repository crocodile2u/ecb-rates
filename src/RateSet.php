<?php
declare(strict_types=1);

namespace ECBRates;

class RateSet implements \Countable
{
    private string $date;
    private array $rates;
    private string $base;

    public function __construct(string $date, array $rates, string $base)
    {
        $this->date = $date;
        $this->rates = array_filter($rates);
        $this->base = $base;
    }

    public function date(): string
    {
        return $this->date;
    }

    public function base(): string
    {
        return $this->base;
    }

    public function has(string $symbol): bool
    {
        return array_key_exists($symbol, $this->rates);
    }

    public function rate(string $symbol): ?float
    {
        if ($symbol === 'EUR') {
            return 1.0;
        }
        return $this->rates[$symbol] ?? null;
    }

    public function all(): array
    {
        return $this->rates;
    }

    public function count()
    {
        return count($this->rates);
    }
}