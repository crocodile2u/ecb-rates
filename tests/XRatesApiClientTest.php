<?php

namespace Tests;

use ECBRates\RateSet;
use ECBRates\XRatesApiClient;
use PHPUnit\Framework\TestCase;

class XRatesApiClientTest extends TestCase
{
    const KNOWN = [
        "2020-11-02" => [
            "date" => "2020-11-02",
            "rates" => [
                "CAD" => 1.5466,
                "HKD" => 9.0327,
                "ISK" => 163.5,
                "PHP" => 56.407,
                "DKK" => 7.4455,
                "HUF" => 366.24,
                "CZK" => 27.131,
                "AUD" => 1.6533,
                "RON" => 4.8674,
                "SEK" => 10.3625,
                "IDR" => 17064.82,
                "INR" => 86.7555,
                "BRL" => 6.6916,
                "RUB" => 93.745,
                "HRK" => 7.5695,
                "JPY" => 121.93,
                "THB" => 36.249,
                "CHF" => 1.0695,
                "SGD" => 1.5903,
                "PLN" => 4.6018,
                "BGN" => 1.9558,
                "TRY" => 9.8332,
                "CNY" => 7.7962,
                "NOK" => 11.1128,
                "NZD" => 1.7565,
                "ZAR" => 18.8972,
                "USD" => 1.1652,
                "MXN" => 24.7327,
                "ILS" => 3.9681,
                "GBP" => 0.90053,
                "KRW" => 1320.61,
                "MYR" => 4.8443,
            ],
        ],
        "2020-10-30" => [
            "date" => "2020-10-30",
            "rates" => [
                "CAD" => 1.5556,
                "HKD" => 9.0706,
                "ISK" => 164.4,
                "PHP" => 56.635,
                "DKK" => 7.4466,
                "HUF" => 367.45,
                "CZK" => 27.251,
                "AUD" => 1.6563,
                "RON" => 4.8725,
                "SEK" => 10.365,
                "IDR" => 17108.33,
                "INR" => 87.1115,
                "BRL" => 6.7607,
                "RUB" => 92.4606,
                "HRK" => 7.5748,
                "JPY" => 122.36,
                "THB" => 36.439,
                "CHF" => 1.0698,
                "SGD" => 1.5952,
                "PLN" => 4.6222,
                "BGN" => 1.9558,
                "TRY" => 9.794,
                "CNY" => 7.8158,
                "NOK" => 11.094,
                "NZD" => 1.7565,
                "ZAR" => 19.0359,
                "USD" => 1.1698,
                "MXN" => 24.8416,
                "ILS" => 3.9881,
                "GBP" => 0.90208,
                "KRW" => 1324.2,
                "MYR" => 4.8588,
            ],
        ]
    ];

    public function testOnPreviousBusinessDay()
    {
        $client = new XRatesApiClient();
        $rs = $client->onPreviousBusinessDay(new \DateTime("2020-11-02"));
        $this->assertInstanceOf(RateSet::class, $rs);
        $this->assertEquals("EUR", $rs->base());
        foreach (self::KNOWN["2020-10-30"]["rates"] as $symbol => $rate) {
            $this->assertEquals($rate, $rs->rate($symbol));
        }
    }

    public function testOn()
    {
        $client = new XRatesApiClient();
        $rs = $client->on(new \DateTime("2020-11-02"));
        $this->assertInstanceOf(RateSet::class, $rs);
        $this->assertEquals("EUR", $rs->base());
        foreach (self::KNOWN["2020-11-02"]["rates"] as $symbol => $rate) {
            $this->assertEquals($rate, $rs->rate($symbol));
        }
    }

    public function testLatest()
    {
        $client = new XRatesApiClient();
        $rs = $client->latest();
        $this->assertInstanceOf(RateSet::class, $rs);
        $this->assertEquals("EUR", $rs->base());
        $this->assertGreaterThan(0, $rs->all());
    }
}
