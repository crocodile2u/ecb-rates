# ECB rates provides historical EUR exchange rates as published by European Central Bank

Requirements:
* ZIP php extension (`"ext-zip": "*"`)
* JSON php extension (`"ext-json": "*"`)

Installation:

```
composer require crocodile2u/ecb-rates
```

Usage:

```php
$provider = new \ECBRates\ExchangeRateProvider($this->createRatesIterator());

// fetch latest available rates, either for today or (which is more likely) for yesterday
$latest = $provider->at(new \DateTime("now"));

// fetch latest rates excluding today, thus making sure that
// throughout of today we're operating with yesterday rates
$previous = $provider->atPreviousBusinessDay(new \DateTime("now"));

$historical = $provider->at(new \DateTime("2019-02-01"));
```

Methods `at()` and `atPreviousBusinessDay()` both return a `RateSet` object with the following methods:

* `date()` - returns string representation of date (Y-m-d) for which this RateSet is valid
* `rate(string $symbol)` - returns a float rate for $symbol or NULL if it's not found
* `has(string $symbol)` - returns bool whether the rate for $symbol exists in this set
* `all()` - returns all rates in this set as an associative array(string symbol => float rate)
* `count()` - return int number of rates in this set